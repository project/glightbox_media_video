# CONTENTS OF THIS FILE

- Introduction
- Features
- Requirements
- Installation
- Maintainers

## Introduction

GLightbox Media Video extends Drupal GLightbox module with support for core media video and remote video types.

## Features

The GLightbox Media Video module:

- Works as a Formatter for Video URL field in Remote Media Video
- Choose between a text or thumbnail to launch a GLightbox
- Supports Media thumbnail and image styles
- Supports Gallery (video grouping) - aka GLightbox rel

## Requirements

- GLightbox (https://www.drupal.org/project/glightbox)
- Media (core)

## Installation

1. Install the module as normal, see link for instructions.
   Link: https://www.drupal.org/documentation/install/modules-themes/modules-8

2. Go to "Media types" -> "Remote Video" -> "Manage display" -> "Video URL" => select GLightbox Remote Media Video formatter and adjust settings

## Recommended modules

Looking for Image gallery module? You can try these modules with GLightbox integration:

[Extra Block Types (EBT): Image Gallery](https://www.drupal.org/project/ebt_image_gallery)
[Extra Paragraph Types (EPT): Image Gallery](https://www.drupal.org/project/ept_image_gallery)
[Extra Block Types (EBT): Image](https://www.drupal.org/project/ebt_image)
[Extra Paragraph Types (EPT): Image](https://www.drupal.org/project/ept_image)

Looking for Video gallery module? You can try these modules with GLightbox integration:

[Extra Block Types (EBT): Video and Image Gallery](https://www.drupal.org/project/ebt_video_and_image_gallery)
[Extra Paragraph Types (EPT): Video and Image Gallery](https://www.drupal.org/project/ept_video_and_image_gallery)
[Extra Block Types (EBT): Video](https://www.drupal.org/project/ebt_video)
[Extra Paragraph Types (EPT): Video](https://www.drupal.org/project/ept_video)

## Maintainers

Maintainers:

- Ivan Abramenko (https://www.drupal.org/u/levmyshkin)
